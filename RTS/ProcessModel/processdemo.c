/*
 * =====================================================================================
 *
 *       Filename:  processdemo.c
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "processmodel.h"
#include <signal.h>

int main(){
	signal(SIGUSR1, sigusr1_handler);
	signal(SIGUSR2, sigusr2_handler);
  	pctx model;
	init(&model);
	printmodel(&model);
	while(1){
		sleep(2);
		testaction(&model);
		step(&model);
		printmodel(&model);
		}
}
