/*
 * =====================================================================================
 *
 *       Filename:  processmodel.c
 *
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "processmodel.h"

static int action = 0; /* globale Variabel der definiert die Ausfuehrung fuer Signalhandler */

void sigusr1_handler(){
	action = 1;
	printf("Action: %d, Prozess wird blokiert\n", action);
}

void sigusr2_handler(){
	action = 2;
	printf("Action: %d, Prozess wird zur Warteschlange aus geblockt\n", action);
}

/* Funktion deren Ausfuehrung ist von action Variabel abhaengig */
void testaction(pctx* ctx){
	process* dummyproc;
	if(action == 1){/* Falls Laufende Prozess zur Blocked werden soll  */
		if( ctx->blocked->start == NULL){ /* Wenn Warteschlange leer Platz sichern */
			ctx->blocked->start = (q_node*)malloc(sizeof(*(ctx->running->start)));
			
			ctx->blocked->start->p = q_remove(ctx->running); /* Aus der Running ins Blocked */
			if (ctx->blocked->start->p != NULL) p_switch_block(ctx->blocked->start->p);
			}
		else if (ctx->blocked != NULL){ 
			dummyproc = (process*)malloc(sizeof(*(ctx->running->start->p)));
			dummyproc = q_remove(ctx->running);
			q_add(ctx->blocked, dummyproc);
			}
		}
	if(action ==2){ /* Falls der Prozess zur Warteschlange soll */
	  	if (ctx->blocked->start == NULL){
			printf("Keine Prozesse in Blockierte liste. Kann nicht machen \n");
			}
		else if (ctx->blocked != NULL){
			p_switch_block(ctx->blocked->start->p);
			q_add(ctx->qready, q_remove(ctx->blocked));
			}	
		}
	action = 0; /* Globale Variabel reseten */
}

void printmodel(pctx *ctx){
	if (ctx == NULL) return;
	printf("Laufende Prozesse: \n");
	q_print(ctx->running);	
	printf("Bereite Prozesse: \n");
	q_print(ctx->qready);
	printf("Blocked Prozesse: \n");
	q_print(ctx->blocked);	
	}

void step(pctx *ctx){
	if (ctx == NULL) return;
	if (ctx->running->start != NULL){
		p_switch_state(ctx->running->start->p); 
		q_add(ctx->qready, ctx->running->start->p); /* Aus der Laufende Prozess an der letzte Stelle der Warteschlange */	
		}
	if (ctx->running->start == NULL){
		ctx->running->start = (q_node*)malloc(sizeof(*(ctx->running->start)));
		}
	ctx->running->start->p = q_remove(ctx->qready); /* Aus der Warteschlange an der erste Stelle der Laufende queue bringen */
	if (ctx->running->start->p != NULL) p_switch_state(ctx->running->start->p);
	}

void init(pctx *ctx){
	if (ctx == NULL) return;
	queue* tempq = (queue*)malloc(sizeof(*tempq)); /* Speicher fuer eine piinter auf queue. Liefert einen void pointer zurueck */
	process* tempproc;
	tempq->start = NULL;
	tempq->end = NULL;
	ctx->qready = tempq;
	ctx->running =  (queue*)malloc(sizeof(*(ctx->running))); 
	ctx->running->start = NULL;
	ctx->running->end = NULL;
	ctx->blocked->start = NULL;
	ctx->blocked->end = NULL;

	for(int i; i <= 10; i++){
		tempproc = (process*)malloc(sizeof(*tempproc));
		tempproc->p_id = i;
		tempproc->p_state = READY;
		q_add(tempq, tempproc);
		}
	step(ctx);
}
