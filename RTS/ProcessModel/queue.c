/*
 * =====================================================================================
 *
 *       Filename:  queue.c
 *
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

void q_add(queue *q, process *proc){
	if (q == NULL) return; /* Keine Queue vorgeben */
	q_node* newnode = (q_node*)malloc(sizeof(q_node)); /* Speicherplatz fuer neue Knote sichern */
	newnode->p = proc;
	newnode->next = NULL;
		
	if (q->end != NULL){ /* Wenn die Queu schon eine letzte hat*/
		q->end->next = newnode; /* Der neue Knote mit der vorletzter 
					Element verbinden */
		}
	q->end = newnode; /* Als der letzter Element hinzufuegen */

	if (q->start == NULL) q->start = newnode; /* Wenn noch keine Nodes in der Queue gibt */
	}

process *q_remove(queue *q){
	if (q == NULL){ 
		printf("No queue to remove from \n");
	       	return NULL;
		}
	process* tempprocess = NULL;
	q_node* tempnode = NULL;
	tempnode = q->start;
	q->start = tempnode->next; 
	tempprocess = tempnode->p;
	free(tempnode);
	return tempprocess;
	}

void q_print(queue *q){
	if (q == NULL) { 
		printf("No queue to print\n");
		return;
		}
  	if (q->start == NULL){
	       	printf("Queue ist leer\n");
		return;
		}
	q_node* testnode = q->start; /* Hilfzeiger erzuegen */
	while(testnode != NULL){
		p_print(testnode->p);
		testnode = testnode->next;
		}
	}
