/*
 * =====================================================================================
 *
 *       Filename:  queue.c
 *
 *
 * =====================================================================================
 */

#include <stdio.h>
#include "queue.h"

int main(){
	queue q1 = { NULL,NULL };
	queue q2 = { NULL,NULL };

	process p2 = { 234, RUNNING};
	process p3 = { 456, READY};
	process p4 = { 789, RUNNING};
	process p5 = { 223, BLOCKED};
	process p6 = { 444, BLOCKED};
	
//	q_node n3 = {NULL, &p3};
//	q_node n2 = {&n3, &p2};

	q_add(&q1, &p2);
	q_add(&q1, &p3);
	q_add(&q1, &p2);
	q_add(&q1, &p4);
	q_add(&q2, &p5);
	q_add(&q2, &p6);
	printf("running queue:\n");
	q_print(&q1);
	printf("blocked queue:\n");
	q_print(&q2);
	printf("Removing from running...\n");
	printf("removed process: ");
	p_print(q_remove(&q1));
	printf("Running Queue that is left: \n");
	q_print(&q1);
	printf("Removing from blocked...\n");
	printf("removed process: ");
	p_print(q_remove(&q2));
	printf("blocked Queue that is left: \n");
	q_print(&q2);

	printf("Removing...\n");
	printf("removed process: ");
	p_print(q_remove(&q1));
	printf("Queuerr that is left: \n");
	q_print(&q1);
	printf("Removing...\n");
	printf("removed process: ");
	p_print(q_remove(&q1));
	printf("Queuerr that is left: \n");
	q_print(&q1);
	return 0;
}
