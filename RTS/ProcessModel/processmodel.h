/*
 * =====================================================================================
 *
 *       Filename:  processmodel.h
 *
 *
 * =====================================================================================
 */


#ifndef PROCESSMODEL_H
#define PROCESSMODEL_H 
#include "queue.h"
#include "process.h"

typedef struct {
	queue* qready;
	queue* running;
	queue* blocked;
	}pctx;

void printmodel(pctx *ctx);
void step(pctx *ctx);
void init(pctx* ctx);
void sigusr1_handler();
void sigusr2_handler();
void testaction(pctx* ctx);

#endif
