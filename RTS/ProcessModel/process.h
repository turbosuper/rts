/*
 * =====================================================================================
 *
 *       Filename:  process.h
 *
 * =====================================================================================
 */
#ifndef PROCESS_H
#define PROCESS_H
#include <sys/types.h>
#include <unistd.h>

typedef enum { 
	READY,
       	RUNNING,
       	BLOCKED	
	} state;

typedef struct {
	pid_t p_id;
	state p_state;
	} process;


void p_switch_state(process *p);
void p_print(process *p);
void p_switch_block(process *p);

#endif
