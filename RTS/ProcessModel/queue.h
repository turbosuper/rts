/*
 * =====================================================================================
 *
 *       Filename:  queue.h
 *
 *
 * =====================================================================================
 */


#ifndef QUEUE_H
#define QUEUE_H 
#include "process.h"

struct q_node{
	struct q_node *next;
	process *p;
};

typedef struct q_node q_node;

typedef struct {
	struct q_node *start;
	struct q_node *end;
} queue;

void q_add(queue *q, process *proc);
process *q_remove(queue *q);
void q_print(queue *q);

#endif
