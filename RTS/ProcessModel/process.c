/*
 * =====================================================================================
 *
 *       Filename:  process.c
 *
 * =====================================================================================
 */
#include <stdio.h>
#include "process.h"


void p_switch_state(process *p){
	
	if( p->p_state == READY)
		p->p_state = RUNNING;
	else p->p_state = READY;
	}

void p_print(process *p){
	printf("PID %u : ", p->p_id);
	if( p->p_state == READY)
		printf("READY\n");
	if( p->p_state == BLOCKED )
		printf("BLOCKED\n");
	else if (p->p_state == RUNNING) printf("RUNNING\n");
	}

void p_switch_block(process *p){
	
	if( p->p_state != BLOCKED)
		p->p_state = BLOCKED;
	else p->p_state = READY;
	}

