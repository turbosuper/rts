/*
 * =====================================================================================
 *
 *       Filename:  ptest.c
 *
 * =====================================================================================
 */
#include "process.h"

int main(){
	process p1 = {123, READY};

	/*p1->p_state = READY;
	p1->p_id = getppid();  */
	p_print(&p1);
	p_switch_state(&p1);
	p_print(&p1);

	return 0;
}
