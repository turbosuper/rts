/*
 * =====================================================================================
 *
 *       Filename:  childwait.c
 *	 Programm der generiert Kinder Prozesse, und die sollten mithilfe SIGCHILD sich
 *	 mit der Elternprozesse kommunizieren koennen.
 *	 Die Anzahl der Kinder Prozesse ist bei kommandozeile gegeben. 
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

static int n = 0;

void usage(){
	fprintf(stderr, "Not enough arguments\n");
	}

void sigchild_handler(){
	n--;
	}

pid_t generatechildren(int num){
	pid_t temp;
	for (int i = num; i>0; i--){

		n++;
		temp = fork();
	
		if(temp == 0 ) {
		       	printf("Child  %d: started (n = %d)\n", getpid(), n);
			sleep(2+n);/* Zeit fuer die Eltern Prozesse zur andere Prozesse zu generieren.
		       		      Dieser Zeit darf nicht gleich sein fuer jede Prozess! sonst wuerden viele
			      	      Prozesse in einer Zeit terminiert, und Signal wuerde nicht fuer jede
				      Prozesse generiert werden kann     */
			return temp;
			}
		}
}

int main(int argc, char *argv[]){
	pid_t pid;
	pid_t temp;
	int num;
	signal(SIGCHLD, sigchild_handler);

	if (argc != 2){
		usage();
		return EXIT_FAILURE;
		}

	num = atoi(argv[1]);
	temp = generatechildren(num);
	
	while (temp > 0 && n != 0) {
				printf("Parent %d: sleep(2) \n", getpid());
				sleep(2);
				pid = wait(NULL); /* Wenn Elternteil - dann marken ob das Kind schon weg ist */
				printf("Child  %d: terminated, n = %d\n", pid, n);
				}

	if (n==0){
		printf("Parent: %d terminated\n", getpid());
		}

	return EXIT_SUCCESS;
}

