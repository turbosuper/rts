/*
 * =====================================================================================
 *
 *       Filename:  zombies.c
 *	 Programm der generiert Zombie Prozesse.
 *	 Die Anzahl der Zombie Prozesse ist bei kommandozeile gegeben. Mit Negative Zahl
 *	 werden so viele wie moeglich Zombie Prozesse generiert. 
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void usage(){
	fprintf(stderr, "Not enough arguments\n");
	}

void generatez(){
	pid_t temp;
	temp = fork();
	if(temp == 0) exit(0);
}

int main(int argc, char *argv[]){
	int num;
	if (argc != 2){
		usage();
		return EXIT_FAILURE;
	}

	num = atoi(argv[1]);
	for (int i=num; i != 0; i--){
			generatez();
		}
	sleep(4);
	return EXIT_SUCCESS;
}

