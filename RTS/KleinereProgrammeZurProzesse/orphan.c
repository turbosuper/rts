/*
 * =====================================================================================
 *
 *       Filename:  orphan
 *       
 *       Programm der zeigt den PID des Kindes, wenn ihre Elterneil tot ist.
 *
 *	 Der Elternteil verlasst der Program mit einen Exithaendler, der druckt 
 *	 Name der Eigentumer aus.	
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

void exithandler(){
	struct passwd *result; /* Struct mit den Name der Users */
	uid_t userid = getuid(); /* Name des Users erzeugen */
	result = getpwuid(userid); /* zu der Benutzer den passende struct holen */
	printf("Goodbye %s\n", result->pw_name); /* Auf der zu der Name des users erzuegte Struct, die Name asudrucken  */

	}

void pidprint(pid_t temp){

	switch (temp){
		case 0: {
			printf("Hello, ich bin ein kind!\n");
			printf("Meine PID: %d, Meine Elternteil: %d\n", getpid(), getppid());
			sleep(3);
			printf("Jetzt ist mein Eltnerteil weg...\n");
			printf("Meine PID: %d, Meine Elternteil: %d\n", getpid(), getppid());
			break;
			}
		case -1:{
			perror("fork()");
			break;
			}	

		default:{
			printf("Ich bin der Elternteil\n");
			printf("Meine PID: %d\n",getpid());
	atexit(exithandler);
			break;
			}
		
	}
}

int main(){
	pid_t temp;

	temp = fork();
	pidprint(temp);
	return 0;
}
