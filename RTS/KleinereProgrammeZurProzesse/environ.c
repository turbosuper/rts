/*
 * =====================================================================================
 *
 *       Filename:  environ.c
 *	 Programm der erlaubt Bearbeitung der Environment variabels.
 *
 * =====================================================================================
 */



#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <errno.h>

void printenvironment(){
	extern char **environ;
	char *pointer  = *environ;
	for (int i = 1; pointer; i++){ /* Durch die den Array iterieren */
		printf("%s \n", pointer);
		pointer = *(environ+i);
		}	
	printf("\n");
	}

void printentry(){
	char *entry;
	char *choice;

	choice = readline("Bitte geben sie wonach sie suchen: ");
	if ((entry = getenv(choice)) == 0){
		printf("Kein Entsprechendes Beitrag in der Liste Gefunden\n");
		}
		else{	
		printf("Zu der gesuchte Wahls von %s ist diese Beitrag gefunden geworden: %s\n", choice, entry);
		}
	free(choice);
	}

void putentry(){
	char *variable;
	char *value;

	variable = readline("Bitte geben sie welche Variabel wollen sie Erstellene: ");
	value = readline("Bitte geben sie was fuer ein Wert die Variabel haben soll: ");
	if (setenv(variable, value, 0) != 0) printf("Fehler, Variabel nicht gesetzt\n");
		else{	
		printf("Erfolg!\n");
		}
	free(variable);
	free(value);
	}

void modifyentry(){
	char *variable;
	char *value;

	variable = readline("Bitte geben sie welche Variabel wollen sie Erstellene: ");
	value = readline("Bitte geben sie was fuer ein Wert die Variabel haben soll: ");
	if (setenv(variable, value, 1) != 0) printf("Fehler, Variabel nicht gesetzt\n");
		else{	
		printf("Erfolg!\n");
		}
	free(variable);
	free(value);
	}

void removeentry(){
	char *variable;

	variable = readline("Bitte geben sie welche Variabel wollen sie Loeschen: ");
	if (unsetenv(variable) != 0) printf("Fehler, Variabel nicht geloescht\n");
		else{	
		printf("Die gewunschte Variabel gibt es nicht mehr!\n");
		}
	free(variable);
	}

int getmenu(){
	int choice = 10;
	if(choice != 0){
		printf("[1]: Print Env list\n");
		printf("[2]: Print Env entry - getenv()\n");
		printf("[3]: Add Env entry - setenv() without overwrite \n");
		printf("[4]: Modify Env entry - setenv() with overwrite\n");
		printf("[5]: Remove Print Env entry - unsetenv() \n");
		printf("[0]: End\n");
		scanf("%d", &choice);
	}
	if (choice == 0) exit(EXIT_SUCCESS);

	return choice;
}


int main(){
	int menuchoice;
	while ((menuchoice = getmenu()) != 0) {
		switch(menuchoice) {
			case 1 : {  printenvironment(); break ; }
			case 2 : {  printentry(); break ; }
			case 3 : {  putentry(); break ; }
			case 4 : {  modifyentry(); break ; }
			case 5 : {  removeentry(); break ; }
		}
	}
	return EXIT_SUCCESS;
}
